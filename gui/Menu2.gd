extends Control


var track_mouse = false
var prev_mouse_pos = Vector2.ZERO

func _ready():
	var selections = ["selection 1", "selection 2", "selection 3"]
	for i in range(len(selections)):
		$Panel/HBoxContainer/HBoxContainer/OptionButton.add_item(
			selections[i], i
		)

func _process(delta):
	if track_mouse:
		var curr_mouse_pos = get_global_mouse_position()
		var mouse_delta = curr_mouse_pos - prev_mouse_pos
		set_position(get_position() + mouse_delta)
		prev_mouse_pos = curr_mouse_pos

func _on_TextureButton_button_up():
	track_mouse = not track_mouse


func _on_TextureButton_button_down():
	prev_mouse_pos = get_global_mouse_position()
	get_parent().move_child(self, get_parent().get_child_count()-1)
	track_mouse = not track_mouse


func _on_Button5_button_up():
	queue_free()


func _on_OptionButton_item_selected(index):
	print(index)
